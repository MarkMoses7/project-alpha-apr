from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from projects.models import Project, ProjectForm


@login_required
def project_list(request):
    projects = Project.objects.all()
    context = {
        'projects': projects
    }
    return render(request, 'projects/project_list.html', context)


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    context = {'project': project}
    return render(request, 'projects/project_detail.html', context)


@login_required
def create_project(request):
    if request.method == 'POST':
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('projects:list_projects')
    else:
        form = ProjectForm()
    context = {'form': form}
    return render(request, 'projects/create_project.html', context)

