from django.db import models
from django import forms


class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField(default='')
    owner = models.ForeignKey(
        "auth.User",
        related_name="projects",
        on_delete=models.CASCADE,
        null=True
    )

    def __str__(self):
        return self.name


class ProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = ['name', 'description', 'owner']
