from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from accounts.forms import LoginForm
from django.contrib.auth.decorators import login_required
from .models import Project
from django.urls import reverse
from django.contrib.auth.forms import UserCreationForm


@login_required
def project_list(request):
    projects = Project.objects.filter(owner=request.user)
    context = {'projects': projects}
    return render(request, 'accounts/project_list.html', context)


def login_view(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('project-list')
    else:
        form = LoginForm()

    return render(request, 'accounts/login.html', {'form': form})


def logout_view(request):
    logout(request)
    return redirect(reverse('login'))


def signup_view(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            password = form.cleaned_data.get('password1')
            user = form.save()
            user = authenticate(username=user.username, password=password)
            login(request, user)
            return redirect('project_list')
    else:
        form = UserCreationForm()
    return render(request, 'registration/signup.html', {'form': form})
