from django import forms
from django.contrib.auth.forms import UserCreationForm


class LoginForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput)


class SignupForm(UserCreationForm):
    password_confirmation = forms.CharField(
        label='Confirm Password',
        widget=forms.PasswordInput,
        max_length=150
    )


class Meta(UserCreationForm.Meta):
    fields = ['username', 'password1', 'password2', 'password_confirmation']
